
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent }        from './pages/home.component';
import { PortfolioComponent }   from './pages/portfolio.component';


export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'portfolio', component: PortfolioComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
