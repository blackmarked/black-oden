import { BlackOdinPage } from './app.po';

describe('black-odin App', function() {
  let page: BlackOdinPage;

  beforeEach(() => {
    page = new BlackOdinPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
